require("dotenv").config();
const axios = require("axios");
const config = require("./config.json");
const fs = require("fs");

const API_KEY = process.env.API_KEY;
const { max_results } = config;
const { output_dir_name, output_file_prefix } = config.channel;

const getChannelId = async username => {
  const url = `https://www.googleapis.com/youtube/v3/channels?key=${API_KEY}&forUsername=${username}&part=id`;
  const response = await axios.get(url);
  return response.data.items[0].id;
};

const getUploadPlaylistId = async id => {
  const url = `https://www.googleapis.com/youtube/v3/channels?part=contentDetails&id=${id}&key=${API_KEY}`;
  const response = await axios.get(url);
  return response.data.items[0].contentDetails.relatedPlaylists.uploads;
};

const getVideoIdsPage = async (last_page_checked, upload_list_id) => {
  if (last_page_checked === "") {
    const url = `https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=${max_results}&playlistId=${upload_list_id}&key=${API_KEY}`;
    const response = await axios.get(url);
    let idList = [];
    response.data.items.map(video => {
      const id = video.snippet.resourceId.videoId;
      idList.push(id);
    });
    const page = {
      nextPage: response.data.nextPageToken,
      previousPage: response.data.previousPageToken,
      idList: idList
    };
    return page;
  } else {
    const url = `https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=${max_results}&pageToken=${last_page_checked}&playlistId=${upload_list_id}&key=${API_KEY}`;
    const response = await axios.get(url);
    let idList = [];
    response.data.items.map(video => {
      const id = video.snippet.resourceId.videoId;
      idList.push(id);
    });
    const page = {
      nextPage: response.data.nextPageToken,
      previousPage: response.data.previousPageToken,
      idList: idList
    };
    return page;
  }
};

const getVideoSnippet = async videoId => {
  const url = `https://www.googleapis.com/youtube/v3/videos?part=snippet&id=${videoId}&key=${API_KEY}`;
  const response = await axios.get(url);
  return response.data.items[0].snippet;
};

const getVideoPageSnippets = async videoIdList => {
  const promises = [];
  videoIdList.map(id => {
    promises.push(getVideoSnippet(id));
  });
  const snippets = await Promise.all(promises);
  return snippets;
};

const createDir = dir => {
  fs.mkdirSync(dir, function(err) {
    if (err) {
      console.error(`Error creating directory: ${err}`);
    }
  });
};

const checkDir = dir => {
  if (!fs.existsSync(`./out/${dir}`)) {
    if (!fs.existsSync("out")) {
      createDir("out");
      createDir(`./out/${dir}`);
    } else {
      createDir(`./out/${dir}`);
    }
  }
  const out = `./out/${dir}`;
  return out;
};

const saveSnippets = (snippets, count, nextPage) => {
  const dir = checkDir(`${output_dir_name}`);
  const prefix = output_file_prefix;
  let path = "";
  if (typeof nextPage === "undefined") {
    path = `${dir}/${prefix}_${count}.json`;
  } else {
    path = `${dir}/${prefix}_${count}_${nextPage}.json`;
  }
  fs.writeFile(path, JSON.stringify(snippets, null, 2), function(err) {
    if (err) {
      console.error(`Error writing file: ${err}`);
    }
  });
};

const updateConfig = () => {
  fs.writeFile("config.json", JSON.stringify(config, null, 2), function(err) {
    if (err) {
      return console.log(`Error writing the config file: ${err}`);
    }
  });
};

const main = async () => {
  let { username, id, upload_list_id, last_page_checked } = config.channel;
  try {
    if (id === "") {
      id = await getChannelId(username);
      config.channel.id = id;
      updateConfig();
    }
    if (upload_list_id === "") {
      upload_list_id = await getUploadPlaylistId(id);
      config.channel.upload_list_id = upload_list_id;
      updateConfig();
    }
    let count = 1;
    while (true) {
      console.log(count);
      let page = await getVideoIdsPage(last_page_checked, upload_list_id);
      if (typeof page.nextPage === "undefined") {
        let snippets = await getVideoPageSnippets(page.idList);
        saveSnippets(snippets, count, page.nextPage);
        break;
      }
      let snippets = await getVideoPageSnippets(page.idList);
      saveSnippets(snippets, count, page.nextPage);
      last_page_checked = page.nextPage;
      config.channel.last_page_checked = last_page_checked;
      updateConfig();
      count++;
    }
  } catch (err) {
    console.error(err);
  }
};

main();
