# Node Youtube API Consumer

Este projeto consiste em criar uma aplicação, utilizando Node.js, capaz de consumir a API do Youtube com o objetivo de buscar pelo meta-dado de todos os vídeos de um determinado canal.

## Getting Started

Para rodar a aplicação, é necessário clonar ou fazer o download do repositório e criar uma **api key** para utilizar a API do Youtube (https://support.google.com/googleapi/answer/6158862?hl=en).

### Prerequisites

Após clonar o repositório, é necessário instalar os módulos npm com o comando abaixo (na pasta do projeto):

```
npm install
```

Após isso, é necessário passar a **api_key** por variável de ambiente e determinar alguns valores no arquivo de configuração:

```
{
  "max_results": "",
  "channel": {
    "id": "",
    "username": "",
    "upload_list_id": "",
    "last_page_checked": "",
    "output_dir_name": "",
    "output_file_prefix": ""
  }
}
```

- **max_results**: Total de resultados retornados por página, mínimo 5 e máximo 50.
- **username**: Username do canal.
- **output_dir_name**: Nome do diretório que será criado para guardar os arquivos de saída.
- **output_file_prefix**: Prefixo que será utilizado para nomear os arquivos de saída.
- **id, upload_list_id, last_page_checked**: Não é necessário preencher, gerados automaticamente.

Feita a configuração, basta rodar:

```
node app.js
```

Ou:

```
npm start
```

Após a primeira execução, as próximas serão sempre a partir da última página checada, caso queira atualizar resultados antigos, será necessário colocar o valor da página inicial desejada ou deixar "", para começar a partir da primeira novamente.

### Exemplo de saída

Distribuição de arquivos:

![alt text](https://i.imgur.com/ZfPaQIo.png "Exemplo de saída")

Aonde o número representa a página lida, e o código o token da próxima página.

Exemplo de meta-dado:

```
[
  {
    "publishedAt": "2018-09-16T13:00:00.000Z",
    "channelId": "UCCuYi5AP3gya6C5mdWYS6CQ",
    "title": "Como fazer Lasanha de Berinjela!",
    "description": "No vídeo de hoje trouxemos um clássico da culinária italiana: LASANHA! Mas, para ser feliz ao lado desse prato maravilhoso não precisamos deixar a alimentação saudável de lado, né? Então nossa masterchef vai te ensinar Como fazer uma lasanha de berinjela que não perde nada pra convencional, a não ser as calorias e carboidratos. Vem conferir! ;)\n\nGostou? Encontre os produtos ideais para essa receita em http://bit.ly/Shoptime__\n\n_____________________________________________________________\n\nPara assistir mais vídeos como esse,  inscreva-se em nosso canal: http://bit.ly/inscrevaseaqui_\n\n- Deixe seu comentário e siga o Shoptime também nas redes sociais:\n\nFacebook: https://www.facebook.com/CanalShoptime\nInstagram: http://instagram.com/canalshoptime",
    "thumbnails": {
      "default": {
        "url": "https://i.ytimg.com/vi/Amcsff6hvj4/default.jpg",
        "width": 120,
        "height": 90
      },
      "medium": {
        "url": "https://i.ytimg.com/vi/Amcsff6hvj4/mqdefault.jpg",
        "width": 320,
        "height": 180
      },
      "high": {
        "url": "https://i.ytimg.com/vi/Amcsff6hvj4/hqdefault.jpg",
        "width": 480,
        "height": 360
      },
      "standard": {
        "url": "https://i.ytimg.com/vi/Amcsff6hvj4/sddefault.jpg",
        "width": 640,
        "height": 480
      },
      "maxres": {
        "url": "https://i.ytimg.com/vi/Amcsff6hvj4/maxresdefault.jpg",
        "width": 1280,
        "height": 720
      }
    },
    "channelTitle": "Shoptime",
    "tags": [
      "shoptime",
      "canal shoptime",
      "lasanha de berinjela",
      "como fazer lasanha de berinjela",
      "lasanha low carb",
      "como fazer lasanha low carb",
      "como fazer lasanha fitness",
      "lasanha saudavel"
    ],
    "categoryId": "24",
    "liveBroadcastContent": "none",
    "localized": {
      "title": "Como fazer Lasanha de Berinjela!",
      "description": "No vídeo de hoje trouxemos um clássico da culinária italiana: LASANHA! Mas, para ser feliz ao lado desse prato maravilhoso não precisamos deixar a alimentação saudável de lado, né? Então nossa masterchef vai te ensinar Como fazer uma lasanha de berinjela que não perde nada pra convencional, a não ser as calorias e carboidratos. Vem conferir! ;)\n\nGostou? Encontre os produtos ideais para essa receita em http://bit.ly/Shoptime__\n\n_____________________________________________________________\n\nPara assistir mais vídeos como esse,  inscreva-se em nosso canal: http://bit.ly/inscrevaseaqui_\n\n- Deixe seu comentário e siga o Shoptime também nas redes sociais:\n\nFacebook: https://www.facebook.com/CanalShoptime\nInstagram: http://instagram.com/canalshoptime"
    },
    "defaultAudioLanguage": "pt"
  }
  ...
]
```
